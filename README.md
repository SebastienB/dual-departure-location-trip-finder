# dual-departure-location-trip-finder

This tool allows search for cheap flight tickets for 2 people to meet anywhere from 2 different locations at specific dates. Several airports are used for each location. Time and price filters are enforced.

This tool uses the API from https://api.skypicker.com/ and Kiwi (owner of the API) in order to find matching flights for people willing to meet from 2 different departure point.
Person 1 can define 3 departure locations with corresponding departure dates/min departure time/max price
Person 2 can define 2 departure locations with corresponding departure dates/min departure time/max price
The scripts performs the following main tasks :
- automatically request all possible flights from these locations to any destination
- removes the potential destinations that are not available for each persons (if available for any of the departure locations of the person it is considered available)
- looks for return solutions to all possible departure points for each person
- removes the potential destinations for which returns may not be available for each persons (if available to any of the departure locations of the person it is considered available)
- displays the detailed results
- use a similar approach in order to look for flights from any of the locations of each person to any of the locations to the second person
- displays the detailed results

Times are always local times.

Prices are provided by skypicker and are not guaranted. They are all given on single trips basis so you will likely get a better price

A lot of code was borrowed from kjam, especially the SkyPickerApi that was only slightly modified https://github.com/kjam/python_flight_search 
Status of the API can be viewed from this URL :  http://status.kiwi.com/

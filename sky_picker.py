from __future__ import unicode_literals, absolute_import, generators, print_function

""" 
This tool uses the API from https://api.skypicker.com/ and Kiwi (owner of the API) in order to find matching flights for people willing to meet from 2 different departure point.
Person 1 can define 3 departure locations with corresponding departure dates/min departure time/max price
Person 2 can define 2 departure locations with corresponding departure dates/min departure time/max price
The scripts performs the following main tasks :
- automatically request all possible flights from these locations to any destination
- removes the potential destinations that are not available for each persons (if available for any of the departure locations of the person it is considered available)
- looks for return solutions to all possible departure points for each person
- removes the potential destinations for which returns may not be available for each persons (if available to any of the departure locations of the person it is considered available)
- displays the detailed results
- use a similar approach in order to look for flights from any of the locations of each person to any of the locations to the second person
- displays the detailed results

Times are always local times.

Prices are provided by skypicker and are not guaranted. They are all given on single trips basis so you will likely get a better price

A lot of code was borrowed from kjam, especially the SkyPickerApi that was only slightly modified https://github.com/kjam/python_flight_search 
Status of the API can be viewed from this URL :  http://status.kiwi.com/
"""

import requests
from datetime import datetime

import pandas as pd

import json

import sys
reload(sys)
sys.setdefaultencoding('utf8')

""" Parameters for the search """

"""Global parameters"""
num_adults = 1											#number of adults traveling (children not supported)
stopoverto = "04:00"									#maximum duration of a stopover when there are connecting flights
maxstopovers = 1										#maximum number of connecting flights

"""Person 1 airport 1"""
p1_origin1 = "SXF" 										#Airport IATA code of the departure airport, can also be the city name (untested)
p1_origin1_departure_date1 = datetime(2018, 12, 7) 		#minimum departure date from this airport for this person
p1_origin1_departure_date2 = datetime(2018, 12, 7) 		#maximum departure date from this airport for this person
p1_origin1_departure_min_time = "17:00" 				#minimum departure time from this airport for this person (not strictly enforced)
p1_origin1_max_price=150								#maximum price for a single trip ticket

"""Person 1 airport 2"""
p1_origin2 = "TXL" 									    #Airport IATA code of the departure airport, can also be the city name (untested)
p1_origin2_departure_date1 = datetime(2018, 12, 7) 		#minimum departure date from this airport for this person
p1_origin2_departure_date2 = datetime(2018, 12, 7) 		#maximum departure date from this airport for this person
p1_origin2_departure_min_time = "17:00" 				#minimum departure time from this airport for this person (not strictly enforced)
p1_origin2_max_price=150								#maximum price for a single trip ticket

"""Person 1 airport 3"""
p1_origin3 = "LEJ" 										#Airport IATA code of the departure airport, can also be the city name (untested)
p1_origin3_departure_date1 = datetime(2018, 12, 7) 		#minimum departure date from this airport for this person
p1_origin3_departure_date2 = datetime(2018, 12, 7) 		#maximum departure date from this airport for this person
p1_origin3_departure_min_time = "14:00" 				#minimum departure time from this airport for this person (not strictly enforced)
p1_origin3_max_price=200								#maximum price for a single trip ticket

"""Person 2 airport 1"""
p2_origin1 = "BOD" 										#Airport IATA code of the departure airport, can also be the city name (untested)
p2_origin1_departure_date1 = datetime(2018, 12, 7) 		#minimum departure date from this airport for this person
p2_origin1_departure_date2 = datetime(2018, 12, 7) 		#maximum departure date from this airport for this person
p2_origin1_departure_min_time = "18:00" 				#minimum departure time from this airport for this person (not strictly enforced)
p2_origin1_max_price=200								#maximum price for a single trip ticket

"""Person 2 airport 2"""
p2_origin2 = "CDG" 										#Airport IATA code of the departure airport, can also be the city name (untested)
p2_origin2_departure_date1 = datetime(2018, 12, 7) 		#minimum departure date from this airport for this person
p2_origin2_departure_date2 = datetime(2018, 12, 7) 		#maximum departure date from this airport for this person
p2_origin2_departure_min_time = "23:59" 				#minimum departure time from this airport for this person (not strictly enforced). Setting it to 23:59 disables this location as a departure point but keeps it as an arrival/return point
p2_origin2_max_price=150								#maximum price for a single trip ticket

"""Return flights search parameters"""
return_date1 = datetime(2018, 12, 9) 					#minimum departure date for all return flights (except visits flights)
return_date2 = datetime(2018, 12, 9) 					#maximum departure date for all return flights (except visits flights)
return_min_time = "09:00" 								#minimum departure time for all return flights (except visits flights)
return_max_arrival_time = "23:59" 						#maximum arrival time for all return flights (except visits flights)
return_max_price=200 									#maximum ticket price for all return flights (except visits flights)

""" Search parameters for flights to visit the other person location"""
visit_travel_date1 = datetime(2018, 12, 7) 		 		#minimum departure date for flights where one persons visits the other one
visit_travel_date2 = datetime(2018, 12, 7) 				#maximum departure date for flights where one persons visits the other one
visit_departure_min_time = "14:00" 						#minimum departure time for flights where one persons visits the other one
visit_max_arrival_time = "23:59" 						#maximum arrival time for flights where one persons visits the other one
visit_max_visit_price=400 								#maximum ticket price for flights where one persons visits the other one

""" Search parameters for return flights to visit the other person location"""
visit_return_travel_date1 = datetime(2018, 12, 9) 		#minimum departure date for return flights where one persons visits the other one
visit_return_travel_date2 = datetime(2018, 12, 9) 		#maximum departure date for return flights where one persons visits the other one
visit_return_departure_min_time = "16:00" 				#minimum departure time for return flights where one persons visits the other one
visit_return_max_arrival_time = "23:59" 				#maximum arrival time for return flights where one persons visits the other one
visit_return_max_visit_price=400 						#maximum ticket price for return flights where one persons visits the other one

""" global Tables that are used in order to store the matches between airline Codes and airline Names"""
airlineCodes=[]											#Do not touch
airlineNames=[]											#Do not touch


class SkyPickerApi(object):
    """ SkyPicker API. """
    def __init__(self):
        """ Initializes the API object with URL attributes. """
        self.base_url = 'https://api.skypicker.com/'
        self.path = ''
        self.param_str = ''

    @property
    def full_url(self):
        """ Returns the full URL for requesting the data. """
        print('{}{}{}'.format(self.base_url, self.path, self.param_str))
        return '{}{}{}'.format(self.base_url, self.path, self.param_str)

    def get_request(self):
        """ Requests the API endpoint and returns the response """
        headers = {'content-type': 'application/json'}
        resp = requests.get(self.full_url, headers=headers)
        return resp.json()

    def load_airlineCodes(self):
        """ Loads the airlines names and codes from the Kiwi API.        """
        self.path = 'airlines'
        self.param_str = ''
        resp=self.get_request()
        global airlineCodes
        global airlineNames
        for airline in resp:
            airlineCodes.append(airline['id'])
            airlineNames.append(airline['name'])     


    def search_flights_defined(self, origin, dests, start_date, max_start_date, departure_min_time, max_arrival_time, max_price,num_passengers,stopoverto,maxstopovers):
        """ Searches for flights given a time range and origin and destination.
        :param origin: string representing the ID or IATA, several values concatenated with a coma are OK
        :param dests: string representing the ID or IATA, several values concatenated with a coma are OK
        :param start_date: datetime representing first possible travel date
        :param max_start_date: datetime representing max possible travel date
        :param departure_min_time: String do not depart before this time Example: 00:00 
        :param max_arrival_time: String do not arrive after this time Example: 00:00 
        :param stopoverto: Integer duration of stopovers
        :param maxstopovers: Integer maximum number of stopovers
        :param max_price: Integer maximum ticket price
        :param num_passengers: integer

        returns JSON response
        """
        self.path = 'flights'
        self.param_str = '?flyFrom=' + \
            '{}&to={}&dateFrom={}&dateTo={}&passengers={}&price_to={}&dtimefrom={}&dtimeto=23:59&atimefrom=00:00&atimeto={}&partner=picky&v=3&stopoverto={}&maxstopovers={}'.format(
                origin,dests,start_date.strftime('%d/%m/%Y'),
                max_start_date.strftime('%d/%m/%Y'),num_passengers,max_price,departure_min_time,max_arrival_time,stopoverto,maxstopovers)

        resp = self.get_request()
        """ DEBUG 
        with open('datab.json', 'w') as outfile:
			json.dump(resp, outfile)
			outfile.close()
        """
        flights = []
        nb_result=0
        for flight in resp.get('data'):
            nb_result+=1
            flight_info = {
                'departure': datetime.utcfromtimestamp(flight.get('dTime')),
                'arrival': datetime.utcfromtimestamp(flight.get('aTime')),
                'price': flight.get('price'),
                'currency': resp.get('currency'),
                'legs': []
            }
            flight_info['duration'] = datetime.utcfromtimestamp(flight.get('aTimeUTC')) - datetime.utcfromtimestamp(flight.get('dTimeUTC'))
            flight_info['duration_hours'] = (flight_info['duration'].total_seconds() / 60.0) / 60.0
            for route in flight['route']:
                flight_info['legs'].append({
                    'carrier': route['airline'],
                    'departure': datetime.utcfromtimestamp(route.get('dTime')),
                    'arrival': datetime.utcfromtimestamp(route.get('aTime')),
                    'from': '{} ({})'.format(route['cityFrom'],
                                             route['flyFrom']),
                    'to': '{} ({})'.format(route['cityTo'], route['flyTo']),
                    'flight_number': route['flight_no'],
                })
            flight_info['carrier'] = ', '.join(set([c.get('carrier') for c
                                                    in flight_info['legs']]))
            flights.append(flight_info)
        print("nb_results:"+str(nb_result))
        return flights


    def search_flights(self, origin, start_date, end_date, departure_min_time, max_price,num_passengers,stopoverto,maxstopovers):
        """ Searches for flights given a time range and origin and destination.
        :param origin: string representing the ID or IATA, several values concatenated with a coma are OK
        :param start_date: datetime representing first possible travel date
        :param end_date: datetime representing last possible travel date
        :param departure_min_time: String do not depart before this time Example: 00:00 
        :param stopoverto: Integer duration of stopovers
        :param maxstopovers: Integer maximum number of stopovers
        :param max_price: Integer maximum ticket price
        :param num_passengers: integer

        returns JSON response
        """
        self.path = 'flights'
        self.param_str = '?flyFrom=' + \
            '{}&dateFrom={}&dateTo={}&passengers={}&price_to={}&dtimefrom={}&dtimeto=23:59&partner=picky&v=3&stopoverto={}&maxstopovers={}'.format(
                origin, start_date.strftime('%d/%m/%Y'),
                end_date.strftime('%d/%m/%Y'),num_passengers,max_price,departure_min_time,stopoverto,maxstopovers)

        resp = self.get_request()
        """ DEBUG 
        with open('data.json', 'w') as outfile:
			json.dump(resp, outfile)
			outfile.close()
        """
        flights = []
        nb_result=0
        for flight in resp.get('data'):
            nb_result+=1
            flight_info = {
                'departure': datetime.utcfromtimestamp(flight.get('dTime')),
                'arrival': datetime.utcfromtimestamp(flight.get('aTime')),
                'price': flight.get('price'),
                'currency': resp.get('currency'),
                'legs': []
            }
            flight_info['duration'] = datetime.utcfromtimestamp(flight.get('aTimeUTC')) - datetime.utcfromtimestamp(flight.get('dTimeUTC'))
            flight_info['duration_hours'] = (flight_info['duration'].total_seconds() / 60.0) / 60.0
            for route in flight['route']:
                flight_info['legs'].append({
                    'carrier': route['airline'],
                    'departure': datetime.utcfromtimestamp(route.get('dTime')),
                    'arrival': datetime.utcfromtimestamp(route.get('aTime')),
                    'from': '{} ({})'.format(route['cityFrom'],
                                             route['flyFrom']),
                    'to': '{} ({})'.format(route['cityTo'], route['flyTo']),
                    'flight_number': route['flight_no'],
                })
            flight_info['carrier'] = ', '.join(set([c.get('carrier') for c
                                                    in flight_info['legs']]))
            flights.append(flight_info)
        print("nb_results:"+str(nb_result))
        return flights



def prepare_sp_data(results):
    """ Prepare skypicker results so they can be easily compared. """
    sp_df = pd.DataFrame(results)
    sp_df['search_engine'] = 'SkyPicker'
    try:
        sp_df['num_stops'] = sp_df['legs'].map(lambda x: len(x) - 1)
    except:
        print("empty results :"+str(len(sp_df.index)))
    return sp_df


def prepare_final(final_df):
    """ Some cleaning for easy sort and comparison. """
    try:
        final_df['price'] = final_df.price.astype(float)
        final_df['duration_hours'] = final_df.duration_hours.astype(float)
    except:
        print("empty results :"+str(len(sp_df.index)))    
    return final_df

def carrierCodetoName(aString):
    """ Returns the Full carrier name from the Airline code
    :param aString: String carrier code. Exemple "U2" for Easyjet or "FR" for Ryanair
    """
    global airlineCodes
    global airlineNames
    for i in range(len(airlineCodes)):
		if airlineCodes[i]==aString:
			return airlineNames[i]
    return "Unknown"

def displayTravel(atrip):
    """ Nicely displays the details of a trip.
    :param atrip: Pandaframe line
    """
    print("Price:"+str(atrip['price'])+atrip['currency']+" departure:"+str(atrip['departure'])+" arrival:"+str(atrip['arrival'])+" duration:"+str(atrip['duration_hours'])+" nb  of stops:"+str(atrip['num_stops']))
    for leg in atrip['legs']:
        print("\t\t From "+leg['from'].encode('utf-8')+" to "+leg['to'].encode('utf-8')+" departure "+str(leg['departure'])+" arrival "+str(leg['arrival'])+" company "+carrierCodetoName(leg['carrier']).encode('utf-8')+" flight "+leg['carrier']+str(leg['flight_number']))

def extractIATA(aString):
    """ Extracts the IATA name of a destination from the form "destName(IATA)"
    :param aString: String Destination name
    """
    res=aString.split('(')[1].split(')')[0]
    return res

if __name__ == '__main__':
    """ load airline codes to name translation"""
    sp_api = SkyPickerApi()
    sp_api.load_airlineCodes()
	
    """ looking for deartures from P1 origin 1 airport"""
    sp_results = sp_api.search_flights(p1_origin1, p1_origin1_departure_date1, p1_origin1_departure_date2, p1_origin1_departure_min_time, p1_origin1_max_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p1_final1 = prepare_final(sp_df)
    """ DEBUG
    trip_CSV = p1_origin1+str(p1_origin1_departure_date1)+"_"+str(p1_origin1_departure_date2)+".csv"
    p1_final1.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ looking for deartures from P1 origin 2 airport"""
    sp_results = sp_api.search_flights(p1_origin2, p1_origin2_departure_date1, p1_origin2_departure_date2, p1_origin2_departure_min_time, p1_origin2_max_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p1_final2 = prepare_final(sp_df)
    """ DEBUG
    trip_CSV = p1_origin2+str(p1_origin2_departure_date1)+"_"+str(p1_origin2_departure_date2)+".csv"
    p1_final2.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ looking for deartures from P1 origin 3 airport""" 
    sp_results = sp_api.search_flights(p1_origin3, p1_origin3_departure_date1, p1_origin3_departure_date2, p1_origin3_departure_min_time, p1_origin3_max_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p1_final3 = prepare_final(sp_df)
    """ DEBUG
    trip_CSV = p1_origin3+str(p1_origin3_departure_date1)+"_"+str(p1_origin3_departure_date2)+".csv"
    p1_final3.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ looking for deartures from P2 origin 1 airport"""
    sp_results = sp_api.search_flights(p2_origin1, p2_origin1_departure_date1, p2_origin1_departure_date2, p2_origin1_departure_min_time, p2_origin1_max_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p2_final1 = prepare_final(sp_df)
    """ DEBUG
    trip_CSV = p2_origin1+str(p2_origin1_departure_date1)+"_"+str(p2_origin1_departure_date2)+".csv"
    p2_final1.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ looking for deartures from P2 origin 2 airport"""
    sp_results = sp_api.search_flights(p2_origin2, p2_origin2_departure_date1, p2_origin2_departure_date2, p2_origin2_departure_min_time, p2_origin2_max_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p2_final2 = prepare_final(sp_df)
    """ DEBUG
    trip_CSV = p2_origin2+str(p2_origin2_departure_date1)+"_"+str(p2_origin2_departure_date2)+".csv"
    p2_final2.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """

    """ merging the possible destinations for person 1"""
    p1_destinations=[]
    for resultNB in range(len(p1_final1.index)):
        p1_destinations.append(p1_final1["legs"][resultNB][p1_final1["num_stops"][resultNB]]['to'])
    for resultNB in range(len(p1_final2.index)):
        p1_destinations.append(p1_final2["legs"][resultNB][p1_final2["num_stops"][resultNB]]['to'])
    for resultNB in range(len(p1_final3.index)):
        p1_destinations.append(p1_final3["legs"][resultNB][p1_final3["num_stops"][resultNB]]['to'])

    """ merging the possible destinations for person 2"""        
    p2_destinations=[]
    for resultNB in range(len(p2_final1.index)):
        p2_destinations.append(p2_final1["legs"][resultNB][p2_final1["num_stops"][resultNB]]['to'])
    for resultNB in range(len(p2_final2.index)):
        p2_destinations.append(p2_final2["legs"][resultNB][p2_final2["num_stops"][resultNB]]['to'])

    """ Computing the intersection of destinations available for both people"""		
    common_dests=[]
    for dest1 in p1_destinations:
        for dest2 in p2_destinations:
            if dest1 == dest2:
                common_dests.append(dest1)

    """ Looking for all return flights from any of these places to any of the possible departure points"""
    return_origin=""
    for dest in common_dests:
        return_origin=return_origin+extractIATA(dest)+","
    return_origin=return_origin[:-1]
    return_dest=p1_origin1+","+p1_origin2+","+p1_origin3+","+p2_origin1+","+p2_origin2
    sp_results = sp_api.search_flights_defined(return_origin,return_dest, return_date1,return_date2, return_min_time, return_max_arrival_time, return_max_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    returns = prepare_final(sp_df)
    """ DEBUG
    trip_CSV = "origin-"+return_origin+"_dest-"+return_dest+"_"+str(return_date1)+".csv"
    returns.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ Removing the destination that don't have a return flight for each traveling people"""
    for dest in common_dests:
        arriving1=0
        arriving2=0
        for resultNB in range(len(returns.index)):
            if dest==returns["legs"][resultNB][0]['from']:
                if (p1_origin1==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']) or p1_origin2==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']) or p1_origin3==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to'])):
                    arriving1=1
                if (p2_origin1==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']) or p2_origin2==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to'])):
                    arriving2=1
        if ((arriving1==0) or (arriving2==0)):
            common_dests.remove(dest)
            print("No return from :"+dest+str(arriving1)+str(arriving2))

    """ Looking for flights that allow P1 to visit P2"""
    p1_visit_p2_origin = p1_origin1+","+p1_origin2+","+p1_origin3
    p1_visit_p2_destination = p2_origin1+","+p2_origin2
    sp_results = sp_api.search_flights_defined(p1_visit_p2_origin,p1_visit_p2_destination, visit_travel_date1,visit_travel_date2, visit_departure_min_time, visit_max_arrival_time, visit_max_visit_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p1vp2d_final = prepare_final(sp_df)
    """DEBUG
    trip_CSV = p1_visit_p2_origin+str(visit_travel_date1)+"_"+p1_visit_p2_destination+".csv"
    p1vp2d_final.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """

    """ Looking for return flights that allow P1 to visit P2"""
    p1_visit_p2_origin = p2_origin1+","+p2_origin2
    p1_visit_p2_destination = p1_origin1+","+p1_origin2+","+p1_origin3
    sp_results = sp_api.search_flights_defined(p1_visit_p2_origin,p1_visit_p2_destination, visit_return_travel_date1,visit_return_travel_date2, visit_return_departure_min_time, visit_return_max_arrival_time, visit_return_max_visit_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p1vp2r_final = prepare_final(sp_df)
    """DEBUG
    trip_CSV = p1_visit_p2_origin+str(visit_return_travel_date1)+"_"+p1_visit_p2_destination+".csv"
    p1vp2r_final.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ Looking for flights that allow P2 to visit P1"""
    p2_visit_p1_origin = p2_origin1+","+p2_origin2
    p2_visit_p1_destination = p1_origin1+","+p1_origin2+","+p1_origin3
    sp_results = sp_api.search_flights_defined(p2_visit_p1_origin,p2_visit_p1_destination, visit_travel_date1,visit_travel_date2, visit_departure_min_time, visit_max_arrival_time, visit_max_visit_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p2vp1d_final = prepare_final(sp_df)
    """DEBUG
    trip_CSV = p2_visit_p1_origin+str(visit_travel_date1)+"_"+p2_visit_p1_destination+".csv"
    p2vp1d_final.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """

    """ Looking for return flights that allow P2 to visit P1"""
    p2_visit_p1_origin = p1_origin1+","+p1_origin2+","+p1_origin3
    p2_visit_p1_destination = p2_origin1+","+p2_origin2
    sp_results = sp_api.search_flights_defined(p2_visit_p1_origin,p2_visit_p1_destination, visit_return_travel_date1,visit_return_travel_date2, visit_return_departure_min_time, visit_return_max_arrival_time, visit_return_max_visit_price,  num_adults, stopoverto, maxstopovers)
    sp_df = prepare_sp_data(sp_results)
    p2vp1r_final = prepare_final(sp_df)
    """DEBUG
    trip_CSV = p2_visit_p1_origin+str(visit_return_travel_date1)+"_"+p2_visit_p1_destination+".csv"
    p2vp1r_final.to_csv(trip_CSV, sep=str(u';'), encoding='utf-8')
    """
    
    """ Display the (hopefully many) results"""
    for theDest in common_dests:
        print("*********DESTINATION FOUND : "+theDest)
        print("---P1 Departure From : "+p1_origin1)
        for resultNB in range(len(p1_final1.index)):
            if theDest==p1_final1["legs"][resultNB][p1_final1["num_stops"][resultNB]]['to']:
                displayTravel(p1_final1.iloc[resultNB])
        print("---P1 Departure From : "+p1_origin2)
        for resultNB in range(len(p1_final2.index)):
            if theDest==p1_final2["legs"][resultNB][p1_final2["num_stops"][resultNB]]['to']:
                displayTravel(p1_final2.iloc[resultNB])
        print("---P1 Departure From : "+p1_origin3)
        for resultNB in range(len(p1_final3.index)):
            if theDest==p1_final3["legs"][resultNB][p1_final3["num_stops"][resultNB]]['to']:
                displayTravel(p1_final3.iloc[resultNB])
        print("---P2 Departure From : "+p2_origin1)
        for resultNB in range(len(p2_final1.index)):
            if theDest==p2_final1["legs"][resultNB][p2_final1["num_stops"][resultNB]]['to']:
                displayTravel(p1_final1.iloc[resultNB])
        print("---P2 Departure From : "+p2_origin2)
        for resultNB in range(len(p2_final2.index)):
            if theDest==p2_final2["legs"][resultNB][p2_final2["num_stops"][resultNB]]['to']:
                displayTravel(p2_final2.iloc[resultNB])
        print("---P1 Return To : "+p1_origin1)
        for resultNB in range(len(returns.index)):
            if theDest==returns["legs"][resultNB][0]['from']:
                if p1_origin1==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']):
					displayTravel(returns.iloc[resultNB])
        print("---P1 Return To : "+p1_origin2)
        for resultNB in range(len(returns.index)):
            if theDest==returns["legs"][resultNB][0]['from']:
                if p1_origin2==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']):
					displayTravel(returns.iloc[resultNB])
        print("---P1 Return To : "+p1_origin3)
        for resultNB in range(len(returns.index)):
            if theDest==returns["legs"][resultNB][0]['from']:
                if p1_origin3==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']):
					displayTravel(returns.iloc[resultNB])
        print("---P2 Return To : "+p2_origin1)
        for resultNB in range(len(returns.index)):
            if theDest==returns["legs"][resultNB][0]['from']:
                if p2_origin1==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']):
                    displayTravel(returns.iloc[resultNB])
        print("---P2 Return To : "+p2_origin2)
        for resultNB in range(len(returns.index)):
            if theDest==returns["legs"][resultNB][0]['from']:
                if p2_origin2==extractIATA(returns["legs"][resultNB][returns["num_stops"][resultNB]]['to']):
                    displayTravel(returns.iloc[resultNB])
    print("*********VISITS at Home ")
    print("---P1 Visiting P2 departures:")
    for resultNB in range(len(p1vp2d_final.index)):
        displayTravel(p1vp2d_final.iloc[resultNB])
    print("---P1 Visiting P2 returns:")
    for resultNB in range(len(p1vp2r_final.index)):
        displayTravel(p1vp2r_final.iloc[resultNB])
    print("---P2 Visiting P1 departures:")
    for resultNB in range(len(p2vp1d_final.index)):
        displayTravel(p2vp1d_final.iloc[resultNB])
    print("---P2 Visiting P1 returns:")
    for resultNB in range(len(p2vp1r_final.index)):
        displayTravel(p2vp1r_final.iloc[resultNB])
